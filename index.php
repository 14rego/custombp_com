<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width">
		<meta name="description" content="Custom Bottling and Packaging Website is Coming Soon">
		<link rel="shortcut icon" href="favicon.ico" />
		<!-- STYLES -->
		<link href="style.css" rel="stylesheet" type="text/css" />
		<title>Coming Soon</title>
	</head>
	<body>
		<div id="coming-soon">
			<img src="/images/coming-soon_01.png" alt="custombp.com coming soon" class="top" />
			<div class="contact">
				<p>Blue Springs, MO <b>|</b> Toll Free: 800.947.7866 &emsp; <b>|</b> &emsp; Ashley, IN <b>|</b> Toll Free: 877.401.7195</p>
				<h6><a href="mailto:customerservice@custombp.com">customerservice@custombp.com</a></h6>
			</div>
			<img src="/images/coming-soon_03.jpg" alt="photos" class="bottom" />
		</div>
	</body>
</html>